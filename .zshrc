# sources a file if it exists
function safesource() {
	[ -r "$1" ] && source "$1"
}

## zplug
ZPLUG_HOME=~/.zsh/zplug
safesource /usr/share/zplug/init.zsh
if typeset -f zplug >/dev/null ; then
	zplug load
fi
## end zplug

safesource ~/.zsh/aliases

# custom completion
fpath=(~/.zsh/functions $fpath ~/.zsh/zsh-completions/src)

HISTFILE=~/.zsh_histfile
HISTSIZE=1000
SAVEHIST=1000
# Ignore commands starting with space
# Share history across zsh shells, and ignore dupes
setopt histignorealldups sharehistory
# Remove space-prefixed commands from history (passworrds)
setopt histignorespace
# Use emacs mode
bindkey -e

zstyle :compinstall filename '/home/felipe/.zshrc'

# Completion
autoload -Uz compinit
compinit
compdef _gnu_generic swaks

# Have .. be autocompleted
zstyle ':completion:*' special-dirs ..
# Cache apt, dpkg, etc
if [ -n "$XDG_RUNTIME_DIR" ] ; then
	zstyle ':completion:*' use-cache on
	zstyle ':completion:*' cache-path "$XDG_RUNTIME_DIR/zsh-cache"
fi

# Complete user processes
zstyle ':completion:*:processes' command 'ps -au$USER'

# Color in tab completion
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
# Enable menu selection of tab alternatives
zstyle ':completion:*' menu select
# Automatically rehash
zstyle ':completion:*' rehash true
# Incluir todos los comandos git que no tienen complecion
# por default no los completa...
zstyle ':completion:*:*:git:*' user-commands ${${(M)${(k)commands}:#git-*}/git-/}

# help functions
autoload -U run-help
# There are specializations for some commands (git, sudo, etc), load them all
for p in $fpath ; do
	if ! [ -d "$p" ]; then
		continue
	fi
	find $p -name 'run-help-*' | xargs --max-lines=1 --no-run-if-empty basename | while read func ; do
		autoload $func
	done
	unset func
done
unset p
alias help="run-help"

safesource ~/.zsh/setprompt
safesource ~/.zsh/setup-keys
safesource ~/.zsh/dir-hashes

# ctrl-left ctrl-right move word in line
bindkey '^[[1;5D'   backward-word # ctrl+left
bindkey '^[[1;5C'   forward-word  # ctrl+right

# when searching, up and down cycle through searches
if [ -n "${key[Up]}" -a -n "${key[Down]}" ] ; then
	bindkey -M isearch "${key[Up]}" history-incremental-search-backward
	bindkey -M isearch "${key[Down]}" history-incremental-search-forward
fi


# zsh chdir
# Poner email debian para git cuando trabajamos en un paquete debian
chpwd() {
	if ! type git &>/dev/null || git config --local user.email &>/dev/null ; then
		unset GIT_{AUTHOR,COMMITTER}_EMAIL
		return
	fi
	local d
	d=.
	while ! mountpoint -q "$d" && [ "$(/bin/readlink -e $d)" != "/" ] ; do
		[ ! -d "$d"/debian -a ! -f "$d"/debian/changelog ] || break
		d="$d"/..
	done
	if [ -f "$d"/debian/changelog ] ; then
		export GIT_{AUTHOR,COMMITTER}_EMAIL=fsateler@debian.org
	elif pwd | grep -q 'yii2\?-' ; then
		unset  GIT_{AUTHOR,COMMITTER}_EMAIL
	elif pwd | grep -q '^/home/felipe/src/satelabs/buk' ; then
		export GIT_{AUTHOR,COMMITTER}_EMAIL=fsateler@buk.cl
	elif pwd | grep -q '^/home/felipe/src/satelabs/' ; then
		export GIT_{AUTHOR,COMMITTER}_EMAIL=fsateler@sateler.com
	else
		unset  GIT_{AUTHOR,COMMITTER}_EMAIL
	fi
}
# Ejecutamos una vez el chpwd para cuando iniciamos el zsh (por ejemplo por 
# c+a+t en gnome-terminal) en un directorio 
chpwd

function killjob()
{
    emulate -L zsh
    for jobnum in $@ ; do
        kill ${${jobstates[$jobnum]#*:*:}%=*}
    done
}


# added by travis gem
[ -f $HOME/.travis/travis.sh ] && source $HOME/.travis/travis.sh

if [ -n "$TILIX_ID" ] || [  -n "$VTE_VERSION" ] && [ -f /etc/profile.d/vte-2.91.sh ] ; then
        source /etc/profile.d/vte-2.91.sh
fi

# zsh parameter completion for the dotnet CLI
_dotnet_zsh_complete()
{
  local completions=("$(dotnet complete "$words")")

  reply=( "${(ps:\n:)completions}" )
}
compctl -K _dotnet_zsh_complete dotnet


true
